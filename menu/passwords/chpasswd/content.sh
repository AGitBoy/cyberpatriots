#!/bin/bash
# updates the default user password setting
#
# Copyright (C) Avalon Williams
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


[[ -f $COMMON ]] && source $COMMON

while true; do
	newpass=$(dialog --stdout --insecure \
		--passwordbox "New Password:" 8 50) || break

	newpasschk=$(dialog --stdout --insecure \
		--passwordbox "Confirm New Password:" 8 50) || break

	if [[ "$newpass" != "$newpasschk" ]]; then
		dialog --msgbox "Passwords do not match" 5 26
	elif [[ -z "$newpass" ]]; then
		dialog --msgbox "Password cannot be empty" 5 29
	else
		echo "$newpass" > ${DIR}/runtime/default-password
		break
	fi
done
