#!/bin/bash
# prerun script for menu
#
# Copyright (C) 2020  <name of copyright holder>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


[[ -f $COMMON ]] && source $COMMON

# install dependencies
if hasprog apt-get && ! hasprog dialog; then
	sudo apt-get install dialog -y
fi

# ensure runtime directories exist
mkdir -p ${DIR}/config

[[ -d ${DIR}/runtime ]] && rm -rf ${DIR}/runtime
cp -r ${DIR}/config ${DIR}/runtime
