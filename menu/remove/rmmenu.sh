#!/bin/bash
# package removal menu
#   $1 = package list source
#   $2 = menu title
#
# Copyright (C) Avalon Williams
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

[[ -f "$COMMON" ]] && source "$COMMON"

pkgs=$(cat "$1" \
    | tr '\n' '\t' \
    | xargs -d '\t' dialog --stdout --checklist "$2" 25 50 50)

if [[ -n "$pkgs" ]]; then
    delpkgs $pkgs 2>&1 | less
fi
