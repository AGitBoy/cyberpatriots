#!/bin/bash
# Service management menu
#
# Copyright (C) Avalon Williams
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

source $COMMON

if ! hasprog systemctl; then
    dialog --msgbox "Service management requires systemd" 5 39
    exit
fi

# list services for xargs
lsservice() {
    # NOTE: this doesn't show static and masked services
    # because they can't be managed via normal means
    systemctl list-unit-files --all --type=service --no-legend \
        | awk '$2 ~ /(enabled|disabled)/ && $1 !~ /.*@.*/ { printf "%s\n%s\n", $1, $2 }'
}

# check service enabled
serviceenabled() {
    sudo systemctl -q is-enabled "$1"
}

# check service active
servicerunning() {
    sudo systemctl -q is-active "$1"
}

# disable and stop service
servicedisable() {
    sudo systemctl stop "$1"
    sudo systemctl disable "$1"
}

# enable and start service
serviceenable() {
    sudo systemctl start "$1"
    sudo systemctl enable "$1"
}

servicesubmenu() {
    clear
    local sel title enabled active
    servicerunning "$1" && active="Running" || active="Stopped"
    serviceenabled "$1" && enabled="Enabled" || enabled="Disabled"

    title="$1 (${enabled}, ${active})"

    sel=$(dialog --stdout --menu "$title" 25 50 50 \
            enable "Enable (and start) the service" \
            disable "Disable (and stop) the service")

    if [[ "$sel" = "enable" ]]; then
        serviceenable "$1"
    elif [[ "$sel" = "disable" ]]; then
        servicedisable "$1"
    fi
}

servicemenu() {
    local sel

    while true; do
        sel=$(lsservice | xargs -d '\n' dialog --stdout --menu 'Manage Services' 40 80 50)

        if [[ $? -eq 0 ]]; then
            servicesubmenu "$sel"
        else
            break
        fi
    done
}

servicemenu
