#!/bin/bash
# Copyright (C) 2022 Avalon Williams
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

export DIR=${1:-$(dirname $0)/menu}
export COMMON=$(realpath $DIR/common.sh)

# source common functions for the menu
[[ -f $COMMON ]] && source $COMMON

# opens menu from directory
menudir() {
	declare -A items
	local items menu title cancel dimensions subdir output
	menu=$(realpath $1)
	title=$(cat ${menu}/title)

	if [[ -s ${menu}/prerun.sh ]]; then
		bash ${menu}/prerun.sh
	fi

	if [[ -s ${menu}/cancel-label ]]; then
		cancel=$(cat ${menu}/cancel-label)
	else
		cancel="Back"
	fi

	if [[ -s ${menu}/dimensions ]]; then
		dimensions="$(cat ${menu}/dimensions)"
	else
		dimensions="25 50 50"
	fi

	for subdir in ${menu}/*/; do
		subdir=${subdir%*/}

		if ! [[ -f ${subdir}/title ]]; then
			continue
		fi

		stitle=$(cat ${subdir}/title)
		if [[ -f ${subdir}/index ]]; then
			items[$stitle]="export SUBDIR=${subdir}; menudir $subdir"
		elif [[ -f ${subdir}/content.sh ]]; then
			items[$stitle]="export SUBDIR=${subdir}; bash ${subdir}/content.sh"
		fi
	done

	if [[ -s "${menu}/index" ]]; then
		while IFS=$'\t' read name cmd; do
			items[$name]="export SUBDIR=${menu}; $cmd"
		done < "${menu}/index"
	fi

	while output=$(dialog --cancel-label "$cancel" --no-items --stdout \
			--menu "$title" $dimensions "${!items[@]}"); do
		eval "${items[$output]}"
	done
}

menudir $DIR
clear
